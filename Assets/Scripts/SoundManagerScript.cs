using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour
{
    public static AudioClip saucerDrag, saucerDrop, ovenTimer, wood;
    static AudioSource audioSrc;
    void Start()
    {
        saucerDrag = Resources.Load<AudioClip>("Sounds/drag saurce");
        saucerDrop = Resources.Load<AudioClip>("Sounds/drop saurce");
        wood = Resources.Load<AudioClip>("Sounds/wood");
        ovenTimer = Resources.Load<AudioClip>("Sounds/oven timer");

        audioSrc = gameObject.GetComponent<AudioSource>();

    }

    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "saucerDrag":
                audioSrc.PlayOneShot(saucerDrag);
                break;
            case "saucerDrop":
                audioSrc.PlayOneShot(saucerDrop);
                break;
            case "wood":
                audioSrc.PlayOneShot(wood);
                break;
            case "ovenTimer":
                audioSrc.PlayOneShot(ovenTimer);
                break;
        }
    }

    public static void PlayButtonSound()
    {
        audioSrc.PlayOneShot(wood);
    }
}
