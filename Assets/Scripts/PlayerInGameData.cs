using UnityEngine;

public class PlayerInGameData : MonoBehaviour
{
    public static PlayerInGameData instance;

    private PlayerSaveData playerSaveData;

    private void Start()
    {

        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            playerSaveData = SaveSystem.LoadPlayerData();
        }

    }

    internal void CheckAndSaveNewRecord(int level, int moves)
    {
        if (playerSaveData.levelInfoVet[level - 1].movesRecord == 0
            || playerSaveData.levelInfoVet[level - 1].movesRecord > moves)
        {
            playerSaveData.levelInfoVet[level - 1].movesRecord = moves;
            MakeLevelPlayable(level + 1);
            SaveSystem.SavePlayerData(playerSaveData);
        }
    }

    private void MakeLevelPlayable(int level)
    {
        if (playerSaveData.levelInfoVet.Length >= level)
        {
            playerSaveData.levelInfoVet[level - 1].stars = 0;
        }
    }

    public int LastLevel()
    {
        return playerSaveData.levelInfoVet.Length;
    }

    public int LastPlayableLevel()
    {
        PlayerSaveData.LevelInfo[] vet = playerSaveData.levelInfoVet;
        for (int i = 0; i < vet.Length; i++)
        {
            if (vet[i].stars == -1)
            {
                return vet[i-1].levelInt;
            }
        }
        return vet[vet.Length - 1].levelInt;
    }

    public PlayerSaveData.LevelInfo[] GetLevelInfoVet()
    {
        return playerSaveData.levelInfoVet;
    }
}
