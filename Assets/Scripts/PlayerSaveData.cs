[System.Serializable]
public class PlayerSaveData
{
    public LevelInfo[] levelInfoVet;
    
    [System.Serializable]
    public class LevelInfo
    {
        public int levelInt; //{ get; private set; }
        public int stars;
        public int movesRecord;
    }
}