using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using static PlayerInGameData;

public class MainMenu : MonoBehaviour
{
    public void PlayGame()
    {
        StateController.level = PlayerInGameData.instance.LastPlayableLevel();
        SceneManager.LoadScene("Level");
    }

}
