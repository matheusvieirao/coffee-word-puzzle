﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelection : MonoBehaviour
{
    [SerializeField] private GameObject levelTextObject;
    [SerializeField] private int page = 1;
    [SerializeField] private int lastPage;
    PlayerSaveData.LevelInfo[] vet;

    void Start()
    {
        lastPage = (PlayerInGameData.instance.LastLevel() / 10) + 1;
        page = (PlayerInGameData.instance.LastPlayableLevel() / 10) + 1;
        vet = PlayerInGameData.instance.GetLevelInfoVet();
        RenderPage();
    }

    private void RenderPage()
    {
        int firstIndex = (page - 1) * 10;
        int lastIndex = (page * 10) - 1;
        if (vet.Length - 1 < lastIndex)
        {
            lastIndex = vet.Length - 1;
        }
        for (int i = firstIndex; i <= lastIndex; i++)
        {
            string textOut = "Level " + vet[i].levelInt.ToString();
            switch (vet[i].stars)
            {
                case (3):
                    textOut += " ★★★ ";
                    break;
                case (2):
                    textOut += " ☆★★ ";
                    break;
                case (1):
                    textOut += " ☆☆★ ";
                    break;
                default:
                    textOut += " ☆☆☆ ";
                    break;
            }

            textOut += "- moves: " + vet[i].movesRecord + ' ';

            if (vet[i].levelInt < 10)
            {
                textOut += "..";
            }

            if (vet[i].stars == -1)
            {
                textOut += "... locked";
            }
            else
            {
                if (vet[i].movesRecord < 10)
                {
                    textOut += "....... play";
                }
                else if (vet[i].movesRecord < 100)
                {
                    textOut += "..... play";
                }
                else
                {
                    textOut += "... play";
                }
            }

            GameObject levelTextGameObject = Instantiate(levelTextObject, gameObject.transform) as GameObject;
            TextMeshProUGUI textComponent = levelTextGameObject.GetComponent<TextMeshProUGUI>();
            textComponent.text = textOut;
            if (vet[i].stars == -1)
            {
                levelTextGameObject.GetComponent<Button>().interactable = false;
            }

            int levelInt = vet[i].levelInt;
            levelTextGameObject.GetComponent<Button>().onClick.AddListener( () => ClickEnterLevel(levelInt) );

        }

    }

    private void ClickEnterLevel(int level)
    {
        SoundManagerScript.PlayButtonSound();
        StateController.level = level;
        SceneManager.LoadScene("Level");
    }

    public void ClickNextPage()
    {
        if (page < lastPage)
        {
            SoundManagerScript.PlayButtonSound();
            page++;
            DestroyAllChildren();
            RenderPage();
        }
    }

    public void ClickPreviousPage()
    {
        if (page > 1)
        {
            SoundManagerScript.PlayButtonSound();
            page--;
            DestroyAllChildren();
            RenderPage();
        }
    }

    private void DestroyAllChildren()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }
}
