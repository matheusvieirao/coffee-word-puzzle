using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragAndDrop : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler, IDropHandler
{
    private RectTransform saucerDrag;
    private CanvasGroup canvasGroup;
    private SaucerManager saucerManager;
    private RectTransform canvas;
    public int trayId;

    private void Start()
    {
        saucerDrag = GameObject.FindGameObjectsWithTag("SaucerDrag")[0].GetComponent<RectTransform>();
        canvasGroup = GameObject.FindGameObjectsWithTag("SaucerDrag")[0].GetComponent<CanvasGroup>();
        saucerManager = GameObject.FindGameObjectWithTag("LevelHandler").GetComponent<SaucerManager>();
        canvas = GameObject.FindGameObjectWithTag("Canvas").GetComponent<RectTransform>();
    }

    public void SetTrayId(int id)
    {
        trayId = id;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (saucerManager.GetQntSaucerOnTray(trayId) > 0)
        {
            saucerManager.BeginDrag();
            saucerManager.RemoveSaucer(trayId);
            canvasGroup.blocksRaycasts = false;

            // Set the saucer aux image that is outside of the trays
            saucerDrag.position = eventData.position;
            saucerDrag.parent.transform.SetAsLastSibling();
            char character = saucerManager.GetDraggedLetter();
            saucerDrag.GetChild(0).GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("alphabet/" + character);

            //SoundManagerScript.PlaySound("saucerDrag");
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (saucerManager.validDrag)
        {
            saucerDrag.anchoredPosition += eventData.delta / canvas.localScale.x;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        saucerManager.DropOnTray(trayId);
        SoundManagerScript.PlaySound("saucerDrag");
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (saucerManager.validDrag)
        {
            saucerManager.OnEndDrag();
            canvasGroup.blocksRaycasts = true;
            saucerDrag.parent.transform.SetAsFirstSibling(); //so it wont be on front of the click area
            saucerManager.validDrag = false;
        }
    }

}
