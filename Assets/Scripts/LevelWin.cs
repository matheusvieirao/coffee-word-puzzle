using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelWin : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    internal void Won(int moves)
    {
        PlayerInGameData.instance.CheckAndSaveNewRecord(StateController.level, moves);
        gameObject.SetActive(true);
        gameObject.transform.SetAsLastSibling();
        gameObject.SetActive(true);
    }


    public void GoToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void RetryLevel()
    {
        SceneManager.LoadScene("Level");
    }

    public void NextLevel()
    {
        if (StateController.level + 1 <= PlayerInGameData.instance.LastLevel())
        {
            StateController.level += 1;
            SceneManager.LoadScene("Level");
        }
        else
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}
