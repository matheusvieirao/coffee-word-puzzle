using UnityEngine;

public class SaucerManager : MonoBehaviour
{
    private LevelHandler lh;
    public bool validDrag;
    public bool dropInsideContainer;
    [SerializeField] private int trayIdDrag;
    private char draggedLetter;

    private void Start()
    {
        lh = GameObject.FindGameObjectsWithTag("LevelHandler")[0].GetComponent<LevelHandler>();
    }

    internal void RemoveSaucer(int trayIdDrag)
    {
        int qntSaucer = GetQntSaucerOnTray(trayIdDrag);
        if (qntSaucer > 0)
        {
            this.trayIdDrag = trayIdDrag;
            draggedLetter = lh.GetLetterAndRemove(trayIdDrag, qntSaucer - 1);
        }
    }

    public void DropOnTray(int trayIdDrop)
    {
        dropInsideContainer = true;
        if (validDrag)
        {
            lh.TryPutSaucerOnTray(trayIdDrag, trayIdDrop, draggedLetter);
        }
    }

    internal char GetDraggedLetter()
    {
        return draggedLetter;
    }

    internal void BeginDrag()
    {
        validDrag = true;
        dropInsideContainer = false;
    }

    public int GetQntSaucerOnTray(int trayId)
    {
        return lh.GetQntSaucerOnTray(trayId);
    }

    internal void OnEndDrag()
    {
        if (!dropInsideContainer)
        {
            lh.PutSaucerOnTray(trayIdDrag, draggedLetter);
        }
    }
}
