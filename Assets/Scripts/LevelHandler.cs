using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelHandler : MonoBehaviour
{
    [SerializeField] private GameObject tray4;
    [SerializeField] private GameObject tray5;
    [SerializeField] private GameObject tray6;
    [SerializeField] private TextMeshProUGUI textWordsToFind;
    [SerializeField] private TextMeshProUGUI movesTMP;
    [SerializeField] private Transform trayContainer;
    private LevelInfo levelInfo;
    private Tray[] trayVet;
    public char[,] letterMatrix;
    [SerializeField] private LevelWin levelWin;
    [SerializeField] private int moves;

    [System.Serializable]
    public class LevelInfo
    {
        public int sizeTray;
        public int qntTrays;
        public string[] wordsMixed;
        public string[] wordsSorted;
    }

    [System.Serializable]
    public class Tray
    {
        public GameObject trayGameObj;
        public int qntSaucer; //quantity of saucers on the tray at the moment
        public GameObject[] saucerVet;
    }

    internal void TryPutSaucerOnTray(int trayIdDrag, int trayIdDrop, char draggedLetter)
    {

        if (ThereIsEmptySpaceOnTray(trayIdDrop) && trayIdDrop != trayIdDrag)
        {
            PutSaucerOnTray(trayIdDrop, draggedLetter);
            IncrementMove();
            CheckIfWin();
        }
        else
        {
            PutSaucerOnTray(trayIdDrag, draggedLetter);
        }
    }
    public void PutSaucerOnTray(int trayId, char draggedLetter)
    {
        int qntSaucer = GetQntSaucerOnTray(trayId);
        PutLetterOnMatrix(trayId, qntSaucer, draggedLetter);
        SetLetterSpriteFromTrayVet(trayId, qntSaucer, draggedLetter);
        ShowSaucerAndLetter(trayId, qntSaucer);
        trayVet[trayId].qntSaucer += 1;
    }

    private void Start()
    {
        levelInfo = new LevelInfo();
        string jsonLevelText = Resources.Load<TextAsset>("Levels/level" + StateController.level.ToString()).text;
        levelInfo = JsonUtility.FromJson<LevelInfo>(jsonLevelText);

        CreateTrays();
        CreateSaucers();
        SetWordsToFind();
        moves = 0;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            levelWin.Won(moves);
        }
    }

    internal int GetQntSaucerOnTray(int trayId)
    {
        return trayVet[trayId].qntSaucer;
    }

    private void CreateTrays()
    {
        GameObject trayGameObj = GetTrayGameObject(levelInfo.sizeTray);
        trayVet = new Tray[levelInfo.qntTrays];
        for (int i = 0; i < levelInfo.qntTrays; i++)
        {
            trayVet[i] = new Tray();
            trayVet[i].trayGameObj = Instantiate(trayGameObj, trayContainer) as GameObject;
            trayVet[i].trayGameObj.GetComponentInChildren<DragAndDrop>().SetTrayId(i);
        }
    }

    private GameObject GetTrayGameObject(int sizeTray)
    {
        switch (sizeTray)
        {
            case (4):
                return tray4;
            case (5):
                return tray5;
            default:
                return tray6;
        }
    }

    private void CreateSaucers()
    {
        letterMatrix = new char[levelInfo.qntTrays, levelInfo.sizeTray];
        for (int i = 0; i < levelInfo.qntTrays; i++)
        {
            trayVet[i].saucerVet = new GameObject[levelInfo.sizeTray];
            trayVet[i].qntSaucer = 0;
            for (int j = 0; j < levelInfo.sizeTray; j++)
            {
                trayVet[i].saucerVet[j] = trayVet[i].trayGameObj.transform.GetComponentInChildren<HorizontalLayoutGroup>().transform.GetChild(j).gameObject;
                char letter = levelInfo.wordsMixed[i][j];
                letterMatrix[i, j] = letter;
                if (letter == ' ')
                {
                    HideSaucerAndLetter(i, j);
                }
                else
                {
                    ShowSaucerAndLetter(i, j);
                    SetLetterSpriteFromTrayVet(i, j, letter);
                    trayVet[i].qntSaucer += 1;
                }
            }
        }
    }

    public void HideSaucerAndLetter(int i, int j)
    {
        trayVet[i].saucerVet[j].GetComponent<Image>().fillAmount = 0;
        trayVet[i].saucerVet[j].transform.GetChild(0).GetComponentInChildren<Image>().fillAmount = 0;
    }

    public void ShowSaucerAndLetter(int i, int j)
    {
        trayVet[i].saucerVet[j].GetComponent<Image>().fillAmount = 1;
        trayVet[i].saucerVet[j].transform.GetChild(0).GetComponentInChildren<Image>().fillAmount = 1;
    }

    public void SetLetterSpriteFromTrayVet(int i, int j, char letter)
    {
        trayVet[i].saucerVet[j].transform.GetChild(0).GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("alphabet/" + letter);
    }

    private void SetWordsToFind()
    {
        string text = "";
        foreach (string word in levelInfo.wordsSorted)
        {
            text = text + "- " + word + "\n";
        }
        textWordsToFind.text = text;
    }
    internal void CheckIfWin()
    {
        List<string> palavrasMontadas = new List<string>();
        for (int i = 0; i < levelInfo.qntTrays; i++)
        {
            palavrasMontadas.Add("");
            for (int j = 0; j < levelInfo.sizeTray; j++)
            {
                palavrasMontadas[i] += letterMatrix[i, j];
            }
            palavrasMontadas[i] = palavrasMontadas[i].Trim(' ', '_');
        }

        bool todasPalavrasCorretas = true;
        foreach (string palavraCorreta in levelInfo.wordsSorted)
        {
            if (!palavrasMontadas.Contains(palavraCorreta)) {
                todasPalavrasCorretas = false;
                break;
            }
        }

        if (todasPalavrasCorretas)
        {
            SoundManagerScript.PlaySound("ovenTimer");
            levelWin.Won(moves);
        }
    }

    // used by SaucerManager to change letter at saucer when dragged
    internal char GetLetterAndRemove(int trayPos, int saucerPos)
    {
        char aux = letterMatrix[trayPos, saucerPos];
        letterMatrix[trayPos, saucerPos] = ' ';
        HideSaucerAndLetter(trayPos, saucerPos);
        trayVet[trayPos].qntSaucer -= 1;
        return aux;
    }

    // used by SaucerManager to change letter at saucer when dragged
    internal void PutLetterOnMatrix(int trayPos, int saucerPos, char letterDraged)
    {
        letterMatrix[trayPos, saucerPos] = letterDraged;
    }

    internal bool ThereIsEmptySpaceOnTray(int trayIdDrop)
    {
        return trayVet[trayIdDrop].qntSaucer < levelInfo.sizeTray;
    }

    internal void IncrementMove()
    {
        moves++;
        movesTMP.text = "MOVES: " + moves;
    }
}
