using System.IO;
using UnityEngine;

public static class SaveSystem
{
    public static PlayerSaveData LoadPlayerData()
    {
        //Application.persistantDataPath
        string path = Application.persistentDataPath + "/player-data.txt";
        string jsonPlayerData = "";
        if (File.Exists(path))
        {
            jsonPlayerData = File.ReadAllText(path);
        }
        // Save file not found so creating a new save file from the template from Resources
        else
        {
            jsonPlayerData = Resources.Load<TextAsset>("player-data-default").text;
            File.WriteAllText(path, jsonPlayerData);
        }

        if (jsonPlayerData == "")
        {
            Debug.LogError("Save file not found at Resources and in " + path);
        }
        return JsonUtility.FromJson<PlayerSaveData>(jsonPlayerData);

    }

    public static void SavePlayerData(PlayerSaveData playerSaveData)
    {
        string path = Application.persistentDataPath + "/player-data.txt";
        string jsonLevelText = JsonUtility.ToJson(playerSaveData);
        File.WriteAllText(path, jsonLevelText);
    }
}
