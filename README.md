# Coffee Word Puzzle

## Desenvolvimento:
Coffee Word Puzzle é um jogo estilo quebra-cabeça para relaxar, para você jogar quando você quiser ficar um pouquinho mais no celular mas quer dar um tempo de rede social e se sentir em um cafeteria aconchegante. 

## Créditos das músicas:
www.bensound.com

## Link do gameplay do jogo:
[![Gameplay do jogo Same Old Snakes](Assets/Thumbnails/coffee-word-puzzle-gameplay-thumbnail.webp)](https://youtu.be/tNV9_beswJs)
